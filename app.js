(function() {
   function createElem(tag, className) {
      let elem = document.createElement(tag);
      elem.className = className;
      return elem;
   }

   let state = {
      todoItems: [],
   };

   function addTodoItem(event) {
      event.preventDefault();
      let name = document.querySelector("#todo-item-name").value;
      let todoItem = {
         name: name,
         dateAdded: new Date().toISOString().split("T")[0],
         completed: false,
         comments: [],
      };

      let options = {
         method: "POST",
         body: JSON.stringify(todoItem),
         headers: {"Content-type": "application/json"}
      };

      fetch("?cmd=add", options)
          .then(response => {
             console.log(response);
             return response.json()
          })
          .then(todoItem => {
             state.todoItems.push(todoItem);
             render();
          });
   }

   function completeTodoItem(id) {
      let todoItem = state.todoItems.find(item => item.id === id);
      todoItem.completed = true;

      let options = {
         method: "POST",
         body: JSON.stringify(todoItem),
         headers: {"Content-type": "application/json"}
      };

      fetch("?cmd=edit", options)
          .then(response => {
             console.log(response);
             return response.json()
          })
          .then(() => {
             render();
          });
   }

   function deleteTodoItem(id) {
      let options = {
         method: "POST",
      };

      fetch(`?cmd=delete&id=${id}`, options)
          .then(response => {
              if (response.ok) {
                 let newState = state.todoItems.filter(item => item.id !== id);
                 state.todoItems = newState;
                 render();
              }
          })
   }

   function buildInputForm() {
      let form = createElem("form", "form-inline");
      let inputDiv = createElem("div", "form-group mx-sm-3 mb-2");

      let label = createElem("label", "sr-only");
      label.setAttribute("for", "todo-item-name");
      label.innerText = "Nimi";

      let nameInput = createElem("input", "form-control");
      nameInput.type = "text";
      nameInput.name = "todo-item-name";
      nameInput.id = "todo-item-name";
      nameInput.placeholder = "Mida vaja teha?";

      inputDiv.appendChild(label);
      inputDiv.appendChild(nameInput);

      let submitButton = createElem("button", "btn btn-primary mb-2");
      submitButton.type = "submit";
      submitButton.innerText = "Lisa";

      form.appendChild(inputDiv);
      form.appendChild(submitButton);
      form.addEventListener("submit", addTodoItem);
      return form;
   }

   function buildEmptyList() {
      let div = createElem("div", "alert alert-primary");
      div.role = "alert";
      div.innerText = "Lisa endale ülesanne";

      return div;
   }

   function buildItemNameLabel(todoItem) {
      let div = createElem("div");
      let nameLink = createElem("a");
      let name = createElem("h5",  "mb-1");

      if (todoItem.completed) {
         name.className += " text-muted";
         let strikeThrough = createElem("s");
         strikeThrough.innerText = todoItem.name;
         name.appendChild(strikeThrough);
      } else {
         name.innerText = todoItem.name;
      }

      nameLink.appendChild(name);
      div.appendChild(nameLink);

      return div;
   }

   function buildCompletedForm(todoItem) {
      let completeForm = createElem("form", "form-inline");
      let completeButton = createElem("input", "btn btn-success");
      completeButton.type = "submit";
      completeButton.value = "Valmis";
      completeForm.appendChild(completeButton);
      completeForm.addEventListener("submit", (event) => {
         event.preventDefault();
         completeTodoItem(todoItem.id);
      });

      return completeForm;
   }

   function buildItemControls(todoItem) {
      let div = createElem("div");
      let dateAdded = createElem("small", "date-added");
      dateAdded.innerText = todoItem.dateAdded;



      let deleteForm = createElem("form", "form-inline");
      let deleteButton = createElem("input", "btn btn-danger");
      deleteButton.type = "submit";
      deleteButton.value = "Kustuta";
      deleteForm.appendChild(deleteButton);
      deleteForm.addEventListener("submit", (event) => {
         event.preventDefault();
         deleteTodoItem(todoItem.id);
      });

      div.appendChild(dateAdded);
      if (!todoItem.completed) {
         div.appendChild(buildCompletedForm(todoItem));
      }
      div.appendChild(deleteForm);

      return div;
   }

   function buildListItemContent(todoItem) {
      let div = createElem("div", "d-flex w-100 justify-content-between");

      let name = buildItemNameLabel(todoItem);
      let controls = buildItemControls(todoItem);
      div.appendChild(name);
      div.appendChild(controls);

      return div;
   }

   function buildListWithItems(todoItems) {
      let list = createElem("ul", "list-group");

      for (let item of todoItems) {
         let listItem = createElem("li", "list-group-item");
         let content = buildListItemContent(item);
         listItem.appendChild(content);
         list.appendChild(listItem);
      }

      return list;
   }

   function buildTodoItemsList(todoItems) {
      if (todoItems.length === 0) {
         return buildEmptyList();
      } else {
         return buildListWithItems(todoItems);
      }
   }

   let render = () => {
      let container = document.querySelector(".container");
      while (container.hasChildNodes()) {
         container.removeChild(container.firstChild);
      }

      let title = createElem("h1");
      title.innerText = "Todo rakendus";
      let form = buildInputForm();
      let todoItemsList = buildTodoItemsList(state.todoItems);
      container.appendChild(title);
      container.appendChild(form);
      container.appendChild(todoItemsList);
   };

   document.addEventListener("DOMContentLoaded", () => {
      fetch("?cmd=view")
          .then(response => response.json())
          .then((items) => {
             console.log(items);
             state.todoItems = items;
             render();
          });
   });
})();