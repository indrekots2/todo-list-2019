create table if not exists todo_item (
    id         int primary key auto_increment,
    name       varchar(255),
    date_added date,
    completed  bool
);

create table comment (
    id           int primary key auto_increment,
    todo_item_id int,
    content      text
)
