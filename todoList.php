<?php

const DATA_FILE = "data.txt";

function addTodoItem($todoItem) {
    $todoItems = getTodoItems();

    $itemExists = false;
    foreach ($todoItems as $item) {
        if ($item->name === $todoItem->name) {
            $itemExists = true;
            break;
        }
    }

    if ($itemExists == false) {
        $todoItemCompleted = $todoItem->completed == true ? "true" : "false";
        $line = "$todoItem->name;$todoItem->dateAdded;$todoItemCompleted";
        file_put_contents(DATA_FILE, $line . PHP_EOL, FILE_APPEND);
    }
}

function getTodoItems() {
    $lines = file(DATA_FILE, FILE_IGNORE_NEW_LINES);
    $todoItems = [];
    foreach ($lines as $line) {
        list($name, $dateAdded, $completed) = explode(";", $line);
        $itemCompleted = $completed == "true" ? true : false;
        $todoItem = new TodoItem($name, $dateAdded, $itemCompleted);
        $todoItems[] = $todoItem;
    }

    return $todoItems;
}

function completeTodoItem($name) {
    $todoItems = getTodoItems();
    deleteAllTodoItems();
    foreach($todoItems as $item) {
        if ($item->name == $name) {
            $item->completed = true;
        }
        addTodoItem($item);
    }
}

function deleteAllTodoItems() {
    file_put_contents(DATA_FILE, "");
}

function deleteTodoItem($name) {
    $todoItems = getTodoItems();
    deleteAllTodoItems();
    foreach($todoItems as $todoItem) {
        if ($todoItem->name != $name) {
            addTodoItem($todoItem);
        }
    }
}
