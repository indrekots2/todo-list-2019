<?php

require_once("TodoItemNotFound.php");

const USERNAME = "root";
const PASSWORD = "password";
const URL = "mysql:host=127.0.0.1;dbname=todo";

function saveTodoItem($todoItem) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("insert into todo.todo_item (name, date_added, completed) 
        values (:name, :dateAdded, :completed)");
    $statement->bindValue(":name", $todoItem->name);
    $statement->bindValue(":dateAdded", $todoItem->dateAdded);
    $statement->bindValue(":completed", (int)$todoItem->completed);

    $statement->execute();
    $todoItemId = $connection->lastInsertId();

    saveComments($todoItem->comments, $todoItemId);

    $todoItem->id = $todoItemId;
    return $todoItem;
}

function saveComments($comments, $todoItemId) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    foreach ($comments as $comment) {
        $statement = $connection->prepare("insert into todo.comment (todo_item_id, content) values(:todoItemId, :content)");
        $statement->bindValue(":todoItemId", $todoItemId);
        $statement->bindValue(":content", $comment);
        $statement->execute();
    }
}

function getTodoItems() {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("select ti.id, ti.name, ti.date_added, ti.completed, c.content from todo.todo_item ti
        left join todo.comment c on c.todo_item_id = ti.id");
    $statement->execute();

    $todoItems = [];
    foreach ($statement as $row) {
        $id = $row["id"];

        if (isset($todoItems[$id])) {
            $todoItem = $todoItems[$id];
            $comment = $row["content"];
            $todoItem->addComment($comment);
        } else {
            $name = $row["name"];
            $dateAdded = $row["date_added"];
            $completed = $row["completed"] === "1" ? true : false;
            $comment = $row["content"];
            $todoItem = new TodoItem($name, $dateAdded, $completed, $id);
            $todoItem->addComment($comment);
        }

        $todoItems[$id] = $todoItem;
    }

    return array_values($todoItems);
}

function completeTodoItem($id) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("update todo.todo_item set completed = true where id = :id");
    $statement->bindValue(":id", $id);

    $statement->execute();
}

function deleteTodoItem($id) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("delete from todo.todo_item where id = :id");
    $statement->bindValue(":id", $id);
    $statement->execute();

    deleteTodoItemComments($id);
}

function deleteTodoItemComments($id) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("delete from todo.comment where todo_item_id = :id");
    $statement->bindValue(":id", $id);

    $statement->execute();
}

function getTodoItemById($id) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("select ti.id, ti.name, ti.date_added, ti.completed, c.content from todo.todo_item ti
        left join todo.comment c on c.todo_item_id = ti.id
        where ti.id = :id");
    $statement->bindValue(":id", $id);
    $statement->execute();

    $todoItem = null;
    foreach ($statement as $row) {
        $id = $row["id"];

        if (isset($todoItem)) {
            $comment = $row["content"];
            $todoItem->addComment($comment);
        } else {
            $name = $row["name"];
            $dateAdded = $row["date_added"];
            $completed = $row["completed"] === "1" ? true : false;
            $comment = $row["content"];
            $todoItem = new TodoItem($name, $dateAdded, $completed, $id);
            $todoItem->addComment($comment);
        }
    }

    return $todoItem;
}

function updateTodoItem($todoItem) {
    if (getTodoItemById($todoItem->id) == null) {
        throw new TodoItemNotFound();
    }

    deleteTodoItemComments($todoItem->id);

    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("update todo.todo_item
      set name = :name, date_added = :dateAdded, completed = :completed
      where id = :id");
    $statement->bindValue("name", $todoItem->name);
    $statement->bindValue("dateAdded", $todoItem->dateAdded);
    $statement->bindValue("completed", (int)$todoItem->completed);
    $statement->bindValue("id", $todoItem->id);
    $statement->execute();

    saveComments($todoItem->comments, $todoItem->id);

    return getTodoItemById($todoItem->id);
}
