<?php
require_once("TodoItem.php");
require_once("mysqlTodoList.php");

error_reporting(0);

if (isset($_GET["cmd"])) {
    $cmd = $_GET["cmd"];
}

function printJson($data) {
    header("Content-type: application/json");
    print json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
}

function printClientError($errors) {
    header("Content-type: application/json");
    http_response_code(400);
    $response = ["errors" => $errors];
    print json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
}

if ($cmd === "view") {
    $todoItems = getTodoItems();
    printJson($todoItems);
} else if ($cmd === "add" && $_SERVER["REQUEST_METHOD"] === "POST") {
    $input = json_decode(file_get_contents("php://input"), true);
    $todoItem = TodoItem::fromAssocArray($input);

    $errors = $todoItem->validate();
    if (empty($errors)) {
        saveTodoItem($todoItem);
        printJson($todoItem);
    } else {
        printClientError($errors);
    }
} else if ($cmd === "delete" && $_SERVER["REQUEST_METHOD"] === "POST") {
    $id = $_GET["id"];
    deleteTodoItem($id);
    http_response_code(204);
} else if ($cmd === "edit" && $_SERVER["REQUEST_METHOD"] === "POST") {
    $input = json_decode(file_get_contents("php://input"), true);
    $todoItem = TodoItem::fromAssocArray($input);

    $errors = $todoItem->validate();
    if (empty($errors)) {
        try {
            $todoItem = updateTodoItem($todoItem);
            printJson($todoItem);
        } catch (TodoItemNotFound $e) {
            http_response_code(404);
        }
    } else {
        printClientError($errors);
    }
} else {
    readfile("main.html");
}

