<?php

class TodoItem {
    public $id;
    public $name;
    public $dateAdded;
    public $completed;
    public $comments;

    public static function fromAssocArray($data) {
        $todoItem = new TodoItem($data["name"], $data["dateAdded"], $data["completed"]);
        foreach ($data["comments"] as $comment) {
            $todoItem->addComment($comment);
        }

        if (isset($data["id"])) {
            $todoItem->id = $data["id"];
        }

        return $todoItem;
    }

    public function __construct($name, $dateAdded = null, $completed = false, $id = null) {
        $this->name = $name;
        $this->completed = $completed;
        $this->dateAdded = $dateAdded;
        $this->id = $id;
        $this->comments = [];
    }

    public function __toString() {
        return "TodoItem{id: $this->id, name: $this->name, completed: $this->completed, dateAdded: $this->dateAdded}";
    }

    public function addComment($comment) {
        $this->comments[] = $comment;
    }

    public function validate() {
        $errors = [];

        if (!isset($this->name)) {
            $errors[] = "Nimi on kohustuslik";
        }

        if (!isset($this->dateAdded)) {
            $errors[] = "Kuupäev on kohustuslik";
        }

        return $errors;
    }
}